import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrService } from './common/toastr.service';

import { EventsAppComponent } from './events-app.component';
import { EventsThumbnailComponent } from './events/event-thumbnail.component';
import { EventsListComponents } from './events/events-list.components';
import { EventService } from './events/shared/event.service';
import { NavBarComponent } from './nav/navbar.components';

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponents,
    EventsThumbnailComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [EventService,ToastrService],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }
